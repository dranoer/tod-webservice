package com.nightmareinc.tod.dto;

public class PackagesDto {
    public int id;
    public boolean ispremium;
    public String category_id;
    public String package_name;
    public String package_image;
}
