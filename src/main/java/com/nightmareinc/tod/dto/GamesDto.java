package com.nightmareinc.tod.dto;

public class GamesDto {
    public int id;
    public String content;
    public int like_count;
    public int dislike_count;
    public int game_type;
    public String min_age;
}
