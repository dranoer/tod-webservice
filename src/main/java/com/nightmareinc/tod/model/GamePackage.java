package com.nightmareinc.tod.model;

import com.nightmareinc.tod.repository.CategoriesRepository;

import javax.persistence.*;

@Entity
public class GamePackage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private boolean ispremium;
    private String sku;
    private String category_id;
    private String package_name;
    private String package_image;

    public boolean isIspremium() {
        return ispremium;
    }

    public String getPackage_image() {
        return package_image;
    }

    public void setPackage_image(String package_image) {
        this.package_image = package_image;
    }

    public void setIspremium(boolean ispremium) {
        this.ispremium = ispremium;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }
}