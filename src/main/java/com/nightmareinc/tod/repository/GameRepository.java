package com.nightmareinc.tod.repository;

import com.nightmareinc.tod.model.Game;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface GameRepository extends CrudRepository<Game, Integer> {

//    @Query(value ="SELECT g.id,g.content,g.like_count,g.dislike_count,g.game_type,gp.category_id FROM game as g INNER JOIN game_package as gp on g.game_package_id = gp.id where ispremium=:ispr",nativeQuery = true)
    @Query(value = "SELECT g.id,g.content,g.like_count,g.dislike_count,g.game_type,c.min_age FROM game g JOIN game_package gp ON g.game_package_id = gp.id JOIN categories c ON gp.category_id = c.id WHERE ispremium=:ispr", nativeQuery = true)
    List<Object[]> getFreeGames(@Param("ispr") int is_premium);

    //,gp.category_id

    @Query(value = "UPDATE game set like_count = like_count+1 WHERE id=:newId", nativeQuery = true)
    @Modifying
    @Transactional
    void postLikeGame(@Param("newId") int id);

    @Query(value = "UPDATE game set dislike_count = dislike_count+1 WHERE id=:newId", nativeQuery = true)
    @Modifying
    @Transactional
    void postDislikeGame(@Param("newId") int id);
}
