package com.nightmareinc.tod.repository;

import com.nightmareinc.tod.model.GamePackage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GamePackagesRepository extends CrudRepository<GamePackage, Integer> {

    List<GamePackage> findByIspremium(boolean ispremium);

    @Query(value ="select gp.id,gp.ispremium,ca.min_age,gp.package_name,gp.package_image from game_package as gp inner join categories as ca on gp.category_id = ca.id where ispremium=:ispr",nativeQuery = true)
    List<Object[]> getPremiumPackages(@Param("ispr") int is_premium);

//    @Query("select u from User u where u.age = ?#{[0]}")
//    List<User> findUsersByAge(int age);

}
