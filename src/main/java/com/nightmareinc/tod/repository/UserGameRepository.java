package com.nightmareinc.tod.repository;

import com.nightmareinc.tod.model.UserGame;
import org.springframework.data.repository.CrudRepository;

public interface UserGameRepository extends CrudRepository<UserGame, Integer> {
}