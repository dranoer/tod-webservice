package com.nightmareinc.tod.repository;

import com.nightmareinc.tod.model.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoriesRepository extends CrudRepository<Category, Integer> {
}
