package com.nightmareinc.tod;

public class NotFoundException extends RuntimeException {

    NotFoundException(Integer id) {
        super("Could not found it > i = " + id);
    }

}
